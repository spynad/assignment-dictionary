; vim:ft=nasm
global _start

extern read_word
extern print_string
extern find_word
extern print_newline
extern exit

%define STDIN 0
%define STDOUT 1
%define STDERR 2
%define BUFFER_CAP 256

section .data
%include 'words.inc'

read_word_error:
db 'read_word error', 10, 0

find_word_not_found:
db 'key not found', 10, 0

section .text

_start:
    sub rsp, BUFFER_CAP
    mov rsi, BUFFER_CAP
    mov rdi, rsp
    call read_word
    test rax, rax
    jz .error_read

    mov rdi, rax
    mov rsi, entry
    call find_word
    test rax, rax
    jz .error_find
    mov rdi, rax
    mov rsi, STDOUT
    call print_string
    call print_newline
    mov rdi, 0
    jmp .restore_stack_and_exit
.error_read:
    mov rdi, read_word_error
    mov rsi, STDERR
    call print_string
    mov rdi, 1
    jmp .restore_stack_and_exit
.error_find:
    mov rdi, find_word_not_found
    mov rsi, STDERR
    call print_string
    mov rdi, 1
.restore_stack_and_exit:
    add rsp, BUFFER_CAP
    call exit
