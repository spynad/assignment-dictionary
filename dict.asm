section .text
global find_word

extern string_length
extern string_equals


; loop through a dict. and find entry with key specified in the second argument
; return record address if found, otherwise return 0
find_word:
; rdi - a pointer to a null term str (key)
; rsi - a pointer to a last entry in dict.
.loop:    
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    test rax, rax
    jnz .found

    mov rsi, [rsi]
    test rsi, rsi
    jz .not_found
    jmp .loop

.not_found:
    xor rax, rax
    ret

.found:
    add rsi, 8
    push rsi
    call string_length
    pop rsi
    add rax, rsi
    inc rax
    ret
